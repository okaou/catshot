/*jslint browser: true, node: true */
/*jslint plusplus: true */
/*global angular*/
/*global $*/
"use strict";

var FileTransfer,
    local = "http://localhost:8080/option=",
    openshift = "http://api-okaou.rhcloud.com/option=",
    host = openshift,
    image,
    options,
    params,
    win,
    fail,
    ft,
    currentLast,
    source,
    time;
angular.module('starter.controllers', [])
    
    /************************** CONTACTS CONTROLLER **********************************/

    .controller('ContactsCtrl', function ($scope, $http, $location, $rootScope) {

        //Function tabcontacts redirect to the contact board and use contacts function.

        $scope.tabcontacts = function () {
            $location.url('/tab/contacts');
            $scope.contacts();
        };

        //Function addcontacts

        $scope.addcontact = function () {
            // Requete d'ajout de contact avec l'email et le member token en parametre afin de vérifier la connexion et l'existance de l'utilisateur
            $http.post(host + 'addcontact', {email: localStorage.getItem('email'), token: localStorage.getItem('token'), emailcontact: $scope.emailcontact})
                .success(function (data) {
                    if (data.error) {
                        //S'il y a une erreur alors l'afficher dans le scope d'erreur
                        $scope.add = data.error;
                        setTimeout(function () {
                            $scope.add = '';
                        }, 5000);
                    }
                })
                .error(function (data) {
                    console.log(data);
                });
        };

        //Function contacts même système de vérification d'identité. La requete permet de charger les contacts de l'utilisateur identifié

        $scope.contacts = function () {
            $http.post(host + 'contacts', {email: localStorage.getItem('email'), token: localStorage.getItem('token')})
                .success(function (data) {
                    if (!data.error) {
                        $rootScope.confirms = data.data.confirm;
                        $rootScope.friends = data.data.friends;
                    } else {
                        $scope.errorcontacts = data.error;
                        setTimeout(function () {
                            $scope.errorcontacts = '';
                        }, 5000);
                    }
                })
                .error(function (data) {
                    console.log(data);
                });
        };

        //On appel cette fonction par defaut

        $scope.contacts();

        //Function de confirmation de demande d'ajout.

        $scope.confirmation = function (confid) {
            $http.post(host + 'confirmation', {email: localStorage.getItem('email'), token: localStorage.getItem('token'), confid: confid})
                .success(function (data) {
                    if (!data.error) {
                        $scope.contacts();
                    } else {
                        $scope.errorcontacts = data.error;
                        setTimeout(function () {
                            $scope.errorcontacts = '';
                        }, 5000);
                    }
                })
                .error(function (data) {
                    console.log(data);
                });
        };

        //Function de refus de demande d'ajout.

        $scope.refus = function (refid) {
            $http.post(host + 'refus', {email: localStorage.getItem('email'), token: localStorage.getItem('token'), refid: refid})
                .success(function (data) {
                    if (!data.error) {
                        $scope.contacts();
                    } else {
                        $scope.errorcontacts = data.error;
                        setTimeout(function () {
                            $scope.errorcontacts = '';
                        }, 5000);
                    }
                })
                .error(function (data) {
                    console.log(data);
                });
        };

        //Function de suppression de contact.

        $scope.deletecontact = function (delid) {
            $http.post(host + 'deletecontact', {email: localStorage.getItem('email'), token: localStorage.getItem('token'), delid: delid})
                .success(function (data) {
                    if (!data.error) {
                        $scope.contacts();
                    } else {
                        $scope.errorcontacts = data.error;
                        setTimeout(function () {
                            $scope.errorcontacts = '';
                        }, 5000);
                    }
                })
                .error(function (data) {
                    console.log(data);
                });
        };
    })
    
    /************************** PHOTO CONTROLLER **********************************/

    .controller('PhotoCtrl', function ($scope, $http, $location, $rootScope) {

        //Function de prise de photo

        $scope.snap = function () {
            function onSuccess(imageURI) {
                //récupération de l'imageURI afin de la stocker dans une balise img le temps de selectionner l'envois.
                image = document.getElementById('image');
                image.src = imageURI;
                $scope.friends();
                console.log('success');
            }

            function onFail() {
                console.log('fail');
            }

            navigator.camera.getPicture(onSuccess, onFail, {quality: 50, targetWidth: 400, targetHeight: 400, correctOrientation: false});
        };

        //Function d'envois de la photo à l'aide de l'objet FileTransfer.

        $scope.sendsnap = function (id) {
            $rootScope.SRC = document.getElementById('image').src;
            options = {
                fileKey: "file",
                fileName: "image.jpeg",
                mimeType: "image/jpeg"
            };

            params = {
                email: localStorage.getItem('email'),
                receiver: id,
                time: $scope.timeval,
                token: localStorage.getItem('token')
            };

            options.params = params;

            win = function win() {
                console.log('success');
            };

            fail = function fail(err) {
                console.log(err);
            };

            ft = new FileTransfer();
            ft.upload($rootScope.SRC, encodeURI(host + "image"), win, fail, options);
        };

        //Function qui charge la liste des contact de l'utilisateur

        $scope.friends = function () {
            $http.post(host + 'contacts', {email: localStorage.getItem('email'), token: localStorage.getItem('token')})
                .success(function (data) {
                    if (!data.error) {
                        $rootScope.sendfriend = data.data.friends;
                    } else {
                        $scope.errorcontacts = data.error;
                    }
                })
                .error(function (data) {
                    console.log(data);
                });
        };
    })

    /************************** RECEPTION CONTROLLER **********************************/

    .controller('ReceptionCtrl', function ($scope, $http, $location) {

        //Function de redirection et d'appel à la fonction de reception

        $scope.tabrecept = function () {
            $location.url('/tab/reception');
            $scope.recept();
        };

        //Function de chargement de la listes des photos reçus

        $scope.recept = function () {
            /*currentLast = $('#reception .list li').first().css({"background-color":"red"});
            console.log(currentLast);*/
            /*setInterval(function () {
              //$('<li class"item"></li>').prependTo('#reception .list');
              $http.post(host + 'updatesnaps', {'email' : localStorage.getItem('email'), 'token' : localStorage.getItem('token')})
              .success(function (data, status, headers, config) {
                if (!data.error) {
                  console.log(data);
                } else {
                  console.log(data);
                }
              })
              .error(function (data, status, headers, config) {
                console.log(data);
              });
            }, 5000);*/
            $http.post(host + 'reception', {email: localStorage.getItem('email'), token: localStorage.getItem('token')})
                .success(function (data) {
                    if (!data.error) {
                        console.log(data);
                        $scope.reception = data.data.snaps;
                        //$scope.viewsnap = true;
                    } else {
                        console.log(data);
                    }
                })
                .error(function (data) {
                    console.log(data);
                });
        };

        //Function de validation de la vue de la photo

        $scope.view = function (snap) {
            $http.post(host + 'view', {email: localStorage.getItem('email'), token: localStorage.getItem('token'), snap: snap})
                .success(function (data) {
                    if (!data.error) {
                        this.viewsnap = false;
                        this.deletesnap = true;
                    } else {
                        console.log(data);
                    }
                })
                .error(function (data) {
                    console.log(data);
                });
        };

        //Function de suppression de la photo du fil de reception

        $scope.delete = function (snap) {
            $http.post(host + 'deletesnap', {email: localStorage.getItem('email'), token: localStorage.getItem('token'), snap: snap})
                .success(function (data) {
                    if (!data.error) {
                        $scope.recept();
                    } else {
                        console.log(data);
                    }
                })
                .error(function (data) {
                    console.log(data);
                });
        };
    })
    
    /************************** AUTH CONTROLLER **********************************/

    .controller('AuthCtrl', function ($scope, $http, $location, $rootScope) {

        //Function de chargement des contacts à la connexion

        $scope.contacts = function () {
            $http.post(host + 'contacts', {email: localStorage.getItem('email'), token: localStorage.getItem('token')})
                .success(function (data) {
                    if (!data.error) {
                        $rootScope.confirms = data.data.confirm;
                        $rootScope.friends = data.data.friends;
                    } else {
                        $scope.errorcontacts = data.error;
                    }
                })
                .error(function (data) {
                    console.log(data);
                });
        };

        if (!localStorage.getItem('token')) {

            //Function d'enregistrement de nouvel utilisateur

            $scope.signin = function () {
                $http.post(host + 'inscription', {email: $scope.email, password: $scope.password})
                    .success(function (data) {
                        if (!data.error) {
                            $scope.error = '';
                            $location.url('/login');
                        } else {
                            $scope.error = data.error;
                        }
                    })
                    .error(function (data) {
                        console.log(data);
                        $scope.error = "Erreur de connexion";
                    });
            };

            //Function de connexion d'un utilisateur

            $scope.login = function () {
                $http.post(host + 'connexion', {email: $scope.logemail, password: $scope.logpassword})
                    .success(function (data) {
                        if (!data.error) {
                            //Stockage de l'email et du token user dans le storage local pour la vérification de connexion
                            localStorage.setItem('email', data.data.email);
                            localStorage.setItem('token', data.token);
                            $scope.error = '';
                            $scope.contacts();
                            $rootScope.sendfriend = null;
                            $location.url('/tab/photo');
                            document.getElementById('image').src = '';
                        } else {
                            $scope.error = data.error;
                        }
                    })
                    .error(function (data) {
                        console.log(data);
                        $scope.error = "Erreur de connexion";
                    });
            };
        } else {

            //Function de suppression de l'email et le token du local storage pour la déconnexion

            $scope.logout = function () {
                localStorage.removeItem('email');
                localStorage.removeItem('token');
                $location.url('/login');
            };

            $location.url('/tab/photo');
        }
    })
    
    //Directive d'ouverture de snap avec le temps limité

    .directive("timer", function () {
        return {
            restrict: 'A',
            link: function(scope, element, attrs){
                if (attrs.state == 1) {
                    element[0].style.backgroundColor = "rgba(0,0,0,0.6)";
                } else {
                    element.children()[0].children[1].addEventListener("touchstart", function () {          
                        source = "http://api-okaou.rhcloud.com/downloads/" + attrs.source;
                        element.children()[1].children[0].src = source;
                        time = attrs.timer * 1000;
                        setTimeout(function () {
                            element.children()[1].children[0].src = "";
                            element.children()[1].children[0].style.display = "none";
                            element[0].style.backgroundColor = "rgba(0,0,0,0.6)";
                        }, time);
                    });
                }
      
            }
        };
    })

    .directive("snapsend", function () {
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                element[0].addEventListener("touchstart", function () {
                    element[0].style.backgroundColor = "rgba(186,119,255,0.6)";
                });
            }
        };
    });