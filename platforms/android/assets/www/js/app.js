// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'starter.controllers', 'starter.services'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleLightContent();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  var connect = function ($q, $location) {
        var deferred = $q.defer();
        if (localStorage.getItem('token')) {
            deferred.resolve();
        } else {
            deferred.reject();
            /*lol = setTimeout(function () {*/
                $location.url('/login');
            /*}, 0);*/
        }
        return deferred.promise;
    };
  var logged = function ($q, $location) {
        var deferred = $q.defer();
        if (!localStorage.getItem('token')) {
            deferred.resolve();
        } else {
            deferred.reject();
            $location.url('/tab/chats');
        }
        return deferred.promise;
    };

  $stateProvider

  // setup an abstract state for the tabs directive
    .state('tab', {
    url: "/tab",
    abstract: true,
    templateUrl: "templates/tabs.html"
  })

  // Each tab has its own nav history stack:

  .state('tab.dash', {
    url: '/dash',
    views: {
      'tab-dash': {
        templateUrl: 'templates/tab-dash.html',
        controller: 'ContactsCtrl'
      }
    },
    resolve: {
      connected : connect
    }
  })

  .state('tab.chats', {
      url: '/chats',
      views: {
        'tab-chats': {
          templateUrl: 'templates/tab-chats.html',
          controller: 'PhotoCtrl'
        }
      },
      resolve: {
        connected : connect
      }
    })
    .state('tab.chat-detail', {
      url: '/chats/:chatId',
      views: {
        'tab-chats': {
          templateUrl: 'templates/chat-detail.html',
          controller: 'ChatDetailCtrl'
        }
      },
      resolve: {
        connected : connect
      }
    })

  .state('tab.account', {
    url: '/account',
    views: {
      'tab-account': {
        templateUrl: 'templates/tab-account.html',
        controller: 'ReceptionCtrl'
      }
    },
    resolve: {
      connected : connect
    }
  })

  .state('login', {
    url: '/login',
    templateUrl: 'templates/login.html',
    resolve: {
      connected : logged
    }
  })

  .state('signin', {
    url: '/signin',
    templateUrl: 'templates/signin.html',
    resolve: {
      connected : logged
    }
  });

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/login');

});
