var FileTransfer;
var local = "http://localhost:8080/option=";
var openshift = "http://api-okaou.rhcloud.com/option=";
var host = openshift;
angular.module('starter.controllers', [])

/*///////////////////////////////////////////////////DASH///////////////////////////*/
.controller('ContactsCtrl', function ($scope, $http, $location, $rootScope) {

  angular.element(document).ready(function () {
    console.log('rafraichit');
  });

  $scope.inscrits = function () {
    $http.post(host + 'toutlemonde', {'email' : localStorage.getItem('email'), 'token' : localStorage.getItem('token')})
    .success(function (data, status, headers, config) {      
      if (!data.error) {
        console.log(data);
        $scope.users = data.data;
      } else {
        $scope.errorusers = data.error;
      }
    })
    .error(function (data, status, headers, config) {
      console.log(data);
    });
  }

  $scope.addcontact = function () {
    $http.post(host + 'addcontact', {'email' : localStorage.getItem('email'), 'token' : localStorage.getItem('token'), 'emailcontact' : $scope.emailcontact})
    .success(function (data, status, headers, config) {
      if (!data.error) {
        console.log(data);
      } else {
        console.log($scope.emailcontact);
        console.log(data);
        $scope.add = data.error;
      }
    })
    .error(function (data, status, headers, config) {
      console.log(data);
    });
  }

  $scope.contacts = function () {
    $http.post(host + 'contacts', {'email' : localStorage.getItem('email'), 'token' : localStorage.getItem('token')})
    .success(function (data, status, headers, config) {
      if (!data.error) {
        console.log(data);
        $rootScope.confirms = data.data.confirm;
        $rootScope.friends = data.data.friends;
      } else {
        console.log(data);
        $scope.errorcontacts = data.error;
      }
    })
    .error(function (data, status, headers, config) {
      console.log(data);
    });
  }

  $scope.contacts();

  $scope.confirmation = function (confid) {
    $http.post(host + 'confirmation', {'email' : localStorage.getItem('email'), 'token' : localStorage.getItem('token'), 'confid' : confid})
    .success(function (data, status, headers, config) {
      if (!data.error) {
        console.log(data);
        $scope.contacts();
      } else {
        console.log(data);
        $scope.errorcontacts = data.error;
      }
    })
    .error(function (data, status, headers, config) {
      console.log(data);
    });
  }

  $scope.refus = function (refid) {
    $http.post(host + 'refus', {'email' : localStorage.getItem('email'), 'token' : localStorage.getItem('token'), 'refid' : refid})
    .success(function (data, status, headers, config) {
      if (!data.error) {
        console.log(data);
        $scope.contacts();
      } else {
        console.log(data);
        $scope.errorcontacts = data.error;
      }
    })
    .error(function (data, status, headers, config) {
      console.log(data);
    });
  }

  $scope.deletecontact = function (delid) {
    $http.post(host + 'deletecontact', {'email' : localStorage.getItem('email'), 'token' : localStorage.getItem('token'), 'delid' : delid})
    .success(function (data, status, headers, config) {
      if (!data.error) {
        console.log(data);
        $scope.contacts();
      } else {
        console.log(data);
        $scope.errorcontacts = data.error;
      }
    })
    .error(function (data, status, headers, config) {
      console.log(data);
    });
  }
})

.controller('PhotoCtrl', function ($scope, Chats, $http, $location, $rootScope) {
  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //
  //$scope.$on('$ionicView.enter', function(e) {
  //});
  
  /*$scope.chats = Chats.all();
  $scope.remove = function(chat) {
    Chats.remove(chat);
  }*/

  $scope.snap = function() {
    //$location.url('/tab/chats');
      function onSuccess(imageURI) {
          //$rootScope.imageSRC = imageURI;
          //$scope.$apply();
          var image;
          image = document.getElementById('image');
          image.src = imageURI;
          //document.getElementById('photo').innerHTML = imageURI;
          //$location.path('tab-chats');
          $scope.friends();
          alert('success');
      }
      function onFail() {
        alert('fail');
        console.log('lol');
      }
      navigator.camera.getPicture(onSuccess, onFail, {quality: 50, targetWidth: 400, targetHeight: 400, correctOrientation: false});
  }

  $scope.sendsnap = function (id) {

    $rootScope.SRC = document.getElementById('image').src;
    console.log($rootScope.SRC);

    options = {
            fileKey: "file",
            fileName: "image.jpeg",
            mimeType: "image/jpeg"
        };

        params = {
            email: localStorage.getItem('email'),
            receiver: id,
            time: $scope.timeval,
            token: localStorage.getItem('token')
        };

        options.params = params;

        win = function win() {
            alert('success');
        };
        fail = function fail(err) {
            //alert('non' + err);
            alert('Photo envoyé' + err);
        };

        ft = new FileTransfer();
        ft.upload($rootScope.SRC, encodeURI(host + "image"), win, fail, options);
  }

  $scope.friends = function () {
    $http.post(host + 'contacts', {'email' : localStorage.getItem('email'), 'token' : localStorage.getItem('token')})
    .success(function (data, status, headers, config) {
      if (!data.error) {
        console.log(data);
        $rootScope.sendfriend = data.data.friends;
      } else {
        console.log(data);
        $scope.errorcontacts = data.error;
      }
    })
    .error(function (data, status, headers, config) {
      console.log(data);
    });
  }

  $scope.tabphoto = function () {
    $location.url('/tab/chats');
    //$scope.friends();
  }

  $scope.cntlist = function () {
    $scope.mail = localStorage.getItem('email');
    $scope.tok = localStorage.getItem('token');
    console.log($scope.mail);
    console.log($scope.tok);
    $http.post(host + 'contacts', {'email' : localStorage.getItem('email'), 'token' : localStorage.getItem('token')})
    .success(function (data, status, headers, config) {
      if (!data.error) {
        console.log(data);
        $scope.cntlst = data.data.friends;
      } else {
        console.log(data);
        $scope.errorcontacts = data.error;
      }
    })
    .error(function (data, status, headers, config) {
      console.log(data);
    });
  }

  $scope.cntlist();
})

.controller('ChatDetailCtrl', function ($scope, $stateParams, Chats) {
  $scope.chat = Chats.get($stateParams.chatId);
})

.controller('ReceptionCtrl', function ($scope, $http, $location) {

  $scope.recept = function () {
    $location.url('/tab/account');
    $http.post(host + 'reception', {'email' : localStorage.getItem('email'), 'token' : localStorage.getItem('token')})
    .success(function (data, status, headers, config) {      
      if (!data.error) {
        console.log(data);
        $scope.reception = data.data.snaps;
        $scope.viewsnap = true;
      } else {
        console.log(data);
      }
    })
    .error(function (data, status, headers, config) {
      console.log(data);
    });
  }

  $scope.view = function (snap) {
    $http.post(host + 'view', { 'email' : localStorage.getItem('email'), 'token' : localStorage.getItem('token'), 'snap' : snap})
          .success(function (data, status, headers, config) {
            if (!data.error) {          
              console.log(data);
              $scope.viewsnap = false;
              $scope.deletesnap = true;
            } else {
              console.log(data);
            }        
          })
          .error(function (data, status, headers, config) {
            console.log(data);
          });
  }

  $scope.delete = function (snap) {
    $http.post(host + 'deletesnap', {'email' : localStorage.getItem('email'), 'token' : localStorage.getItem('token'), 'snap' : snap})
    .success(function (data, status, headers, config) {
      if (!data.error) {
        console.log(data);
        $scope.recept();
      } else {
        console.log(data);
      }
    })
    .error(function (data, status, headers, config) {
      console.log(data);
    });
  }
})

.controller('AuthCtrl', function ($scope, $http, $location, $state, $window, $rootScope) {

  $scope.contacts = function () {
    $http.post(host + 'contacts', {'email' : localStorage.getItem('email'), 'token' : localStorage.getItem('token')})
    .success(function (data, status, headers, config) {
      if (!data.error) {
        console.log(data);
        $rootScope.confirms = data.data.confirm;
        $rootScope.friends = data.data.friends;
      } else {
        console.log(data);
        $scope.errorcontacts = data.error;
      }
    })
    .error(function (data, status, headers, config) {
      console.log(data);
    });
  }

  if (!localStorage.getItem('token')) {
    $scope.signin = function () {
      $http.post(host + 'inscription', { 'email' : $scope.email, 'password' : $scope.password})
      .success(function (data, status, headers, config) {
        if (!data.error) {          
          console.log(data);
          $scope.error = '';
          $location.url('/login');
        } else {
          console.log(data);
          $scope.error = data.error;
        }        
      })
      .error(function (data, status, headers, config) {
        console.log(data);
        $scope.error = "Erreur de connexion";
      });
    };

    $scope.login = function () {
      $http.post(host + 'connexion', { 'email' : $scope.logemail, 'password' : $scope.logpassword })
      .success(function (data, status, headers, config) {
        if (!data.error) {
          console.log(data);
          console.log($scope.logemail);
          localStorage.setItem('email', data.data.email);
          localStorage.setItem('token', data.token);
          $scope.error = '';
          $scope.contacts();
          //$state.go('tab.chats', {}, {reload: true});
          //$window.location.href = "#/tab/chats";
          //document.location.reload();
          $location.url('/tab/chats');
          $rootScope.sendfriend = null;
          document.getElementById('image').src = '';
          //$route.reload();
        } else {
          $scope.error = data.error;
        }
        
      })
      .error(function (data, status, headers, config) {
        console.log(data);
        $scope.error = "Erreur de connexion";
      });
    };

  } else {
    $scope.logout = function () {
      localStorage.removeItem('email');
      localStorage.removeItem('token');
      $location.url('/login');
    };
    $location.url('/tab/dash');
  }
  
})

.directive("timer", function () {
  return {
    restrict: 'A',
    link: function(scope, element, attrs){
      if (attrs.state == 1) {
        element[0].style.backgroundColor = "lightblue";
      } else {
        element.children()[0].children[1].addEventListener("touchstart", function () {
          console.log(attrs.state);
          
          source = "http://api-okaou.rhcloud.com/downloads/" + attrs.source;
          element.children()[1].children[0].src = source;
          time = attrs.timer * 1000;
          setTimeout(function () {            

            element.children()[1].children[0].src = "";
            element.children()[1].children[0].style.display = "none";
            element[0].style.backgroundColor = "lightblue";
          }, time);
        });
      }
      
    }
  };
})

.directive("snapsend", function () {
  return {
    restrict: 'A',
    link: function(scope, element, attrs){
      element[0].addEventListener("touchstart", function () {
        element[0].style.backgroundColor = "green";
      });
    }
  };
});
